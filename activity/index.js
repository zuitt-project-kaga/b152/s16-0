let number = prompt("Input number");

console.log("The number you provided is " + number);

for (let i = number; i > 0; i--) {

    if (i <= 50) {
    	console.log("The current number is at 50. Terminating the loop.")
        break;
    }

    if (i % 10 == 0) {
        console.log('The number is divisible by 10. Skipping the number');
        continue;
    }

    if (i % 5 == 0) {
        console.log(i);
    }

}


let str = "supercalifragilisticexpialidocious."
let strAdded = "";
console.log(str);

for (let i = 0; i < str.length; i++) {
    if (str[i].match(/[aioueAIOUE]/)) {
        continue;
    } else {
        strAdded += str[i];
    }
}
console.log(strAdded);