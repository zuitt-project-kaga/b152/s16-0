function displayMsgToSelf() {
    console.log("Are you still there Masa?");
}

for (let i = 0; i < 10; i++) {
    displayMsgToSelf();
}

function showIntensityAlert(windSpeed) {
    try {
        alert(determineTyphoonIntensity(windSpeed));
    } catch (error) {
        console.log(typeof error);
    } finally {
        alert('Intensity updates will show new alert.');
    }
}

// showIntensityAlert(56);

const num = 100,
    x = "a";

console.log(num);
try {
    throw new Error("You catch error man");
    console.log(num / x);
    console.log(a);
} catch (error) {
    console.log('An error caught.');
    console.log('Error message : ' + error);
} finally {
    // alert('Finally will execute.');
}


function gradeEvaluator(gradeOfNum) {
    try {
        let gradeOfChar;
        if (gradeOfNum >= 90) {
            gradeOfChar = "A";
        } else if (gradeOfNum >= 80) {
            gradeOfChar = "B"
        } else if (gradeOfNum >= 71) {
            gradeOfChar = "C"
        } else if (gradeOfNum <= 70) {
            gradeOfChar = "F"
        } else if (gradeOfNum.length === 1 && gradeOfNum.match(/[a-z]/i)) {
            throw new Error("Input apropriate number");
        }
        console.log("Your Grade Is : " + gradeOfChar);
    } catch (error) {
        alert(error);
    } finally {
        alert("Finish running gradeEvaluator system... Bye");
    }
}
// gradeEvaluator(95);
// gradeEvaluator(82);
// gradeEvaluator(73);
// gradeEvaluator(54);
// gradeEvaluator("a");


let number = 15;
while (number >= 1) {
    console.log(number);
    number--;
}

let letters = "ABCDE";
console.log(letters[2]);
console.log(letters.length);

for (let index = 0; index < letters.length; index++) {
    console.log(letters[index]);
}

let fruits = ["aaa", "bbb", "ccc", "ddd", "eee"]
console.log(fruits);

let cars = [{
        type: "Sedan",
        brand: "Toyota"
    },
    {
        type: "Luxualy Sedan",
        brand: "Rols Royse"
    },
    {
        type: "Hatchback",
        brand: "Mazda"
    },
]

for (let i = 0; i < cars.length; i++) {
    console.log(cars[i].type);
}

let myName = "Kaga MasdAhIro";

for (let i = 0; i < myName.length; i++) {
    if (myName[i].toLowerCase().match(/[aioue]/)) {
        console.log(3);
    } else {
        console.log(myName[i]);
    }
}

for (let a = 20; a > 0; a--) {
    if (a % 2 === 0) {
        continue;
    }
    console.log(a);

    if (a < 10) {
        break;
    }
}




for (let i = 0; i < myName.length; i++) {
    if (myName[i].toLowerCase() === "a") {
        continue;
    } 
    if (myName[i].toLowerCase() == "d") {
    	break;
    }
}



